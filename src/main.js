import Vue from 'vue'
import './plugins/vuetify'
import App from './App.vue'
import equalizer from "vue-equalizer";
import VueRouter from 'vue-router';
import Vuetify from 'vuetify'

 
Vue.use(Vuetify)


import { routes } from './routes';

import { store } from './store/store'

Vue.use(VueRouter);

Vue.config.productionTip = false

const router = new VueRouter({
  routes
});

new Vue({
  render: h => h(App),
  store,
  router,
  components: { equalizer }
}).$mount('#app')
