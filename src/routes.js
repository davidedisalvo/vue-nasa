import HelloWorld from './components/HelloWorld.vue';
import Search from './components/Search.vue';
import FinalPage from './components/FinalPage.vue';
import App from './App.vue';

export const routes = [
    { path: '/helloWorld', component: HelloWorld },
    { path: '', component: Search },
    { path: '/FinalPage', component: FinalPage }

];