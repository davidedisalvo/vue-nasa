import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export const store = new Vuex.Store({
    state: {
        popup: false,
        id : 0,
        title: '',
        newDate: [
            
        ],
        queries: ''


    },
    mutations: {
        showPopup: state => {
            return state.popup = true;
        },

        closePopup: state => {
            return state.popup = false;
        },
        showId (state, id) {
            return this.state.id = id
        },

        showTitle(state, payload) {
            return this.state.title = payload
        },
        setNewImage(state, payload) {
            this.state.newDate.push(payload);
        },
       
        deleteImage(state, payload) {
            this.state.newDate.splice(payload, 1)
        },
        saveQuery(state, payload) {
            this.state.queries = payload;
        },
        deleteAll() {
            this.state.newDate = [];
        }

    },
    actions: {

    }
})